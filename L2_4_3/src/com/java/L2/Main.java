package com.java.L2;

import java.util.Arrays;
import java.util.List;

public class Main {
	/*
	 * Create a java class to create a list of string objects i. Count no.strings
	 * whose length is > 5 ii. Count no.of empty strings iii. Find out empty strings
	 * & store them into new list by using Stream API
	 */

	public static void main(String[] args) {

		List<String> flowerList = Arrays.asList("Tulipa", "", "Begonia", "Rosa", "Lirio", "", "Hortencia", "Jasmin",
				"Gerbera", "", "Crisantemo", "Camomila");

		System.out.println("1 - Strings with more than 5 characters: ");
		flowerList.stream().filter(s -> s.codePoints().count() > 5)//
				.sorted()//
				.forEach(System.out::println);

		int emptyStrings = (int) flowerList.stream().filter(d -> d.isEmpty()).count();
		System.out.println("2 - No. of empty strings: " + emptyStrings);

		System.out.println("3 - Empty Strings list: ");
		flowerList.stream().filter(s -> s.isEmpty()).forEach(System.out::println);
		System.out.println("End of the empty list.");

	}

}
