package com.java.L2;

public class Main {
	/*
	 * Create an interface CharacterOccurrence with int findOccurence(String str,
	 * char c) to find no.of occurrences of a given character within the given
	 * string. Implement findOccurence(String str, char c) method by using Lambda
	 * expression in an implementation class ImplClassWithLambda & invoke it to
	 * display the result on the console. [Hint: str=How was your day today? &
	 * c=a the no.of occurrences of a will be 3]
	 */

	public static void main(String[] args) {

		ImplClassWithLambda lambda = new ImplClassWithLambda();
		String str = "How was your day today?";
		char c = 'w';
		System.out.println(lambda.findOccurence(str, c));

	}

}

interface CharacterOccurrence {
	int findOccurence(String str, char c); // no.of occurrences of a given character within the given string
}

class ImplClassWithLambda implements CharacterOccurrence {

	@Override
	public int findOccurence(String str, char c) {
		int r = (int) str.codePoints().filter(a -> a == c).count();
		return r;
	}

}
