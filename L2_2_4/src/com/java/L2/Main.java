package com.java.L2;

import java.util.Currency;
import java.util.Scanner;

public class Main {
	/*
	 * Write a java program to display list of available currencies, currency codes,
	 * and display the currency symbol based on user currency input. For example,
	 * for USD, the symbol is "$". [Hint : use Currency class]
	 */

	public static void main(String[] args) {

		for (Currency cur : Currency.getAvailableCurrencies()) {
			System.out.print(cur + " ");
		}
		System.out.println("");

		Scanner sc = new Scanner(System.in);
		System.out.print("Please choose one of the Currency codes to display the symbol: ");
		String curCode = sc.nextLine();

		Currency c1 = Currency.getInstance(curCode);

		String displayName = c1.getDisplayName();
		String symb = c1.getSymbol();

		System.out.println("Symbol of " + displayName + " is " + symb);
	}

}