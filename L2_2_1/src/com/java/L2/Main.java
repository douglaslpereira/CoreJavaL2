package com.java.L2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {
	/*
	 * Write a program to Write 1) Date object 2) a Double object and 3) A Long
	 * object to file and again reading it back from file.
	 */

	private static final String MYFILE = "myfile.txt";

	public static void main(String[] args) {

		Date date = new Date();
		DateFormat dt = new SimpleDateFormat("dd/MM/YYYY");
		
		double d = 1_253.36;
		long l = 100_003_250;
		
		String data = dt.format(date) + " - " + d + " - " + l;

		writeMyFile(data);
		System.out.println(readMyFile());

	}

	static void writeMyFile(String data) {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(MYFILE))) {
			bw.write(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static String readMyFile() {
		try (BufferedReader br = new BufferedReader(new FileReader(MYFILE))) {
			String line;
			while ((line = br.readLine()) != null) {
				return line;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
