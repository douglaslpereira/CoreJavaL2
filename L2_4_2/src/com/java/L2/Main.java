package com.java.L2;

public class Main {
	/*
	 * Create an interface WordCount with single abstract method int count(String str) 
	 * to count no.of words in a given string. Implement count(String str)
	 * method by using Lambda expression in an implementation class
	 * MyClassWithLambda & invoke it to display the result on the console.
	 */

	public static void main(String[] args) {

		MyClassWithLambda lambda = new MyClassWithLambda();
		String str = "How was your day today?";
		System.out.println(lambda.count(str));

	}

}

interface WordCount {
	abstract int count(String str);
}


class MyClassWithLambda implements WordCount{

	@Override
	public int count(String str) {
		int count = (int) str.codePoints().count();
		return count;
	}
	
}
