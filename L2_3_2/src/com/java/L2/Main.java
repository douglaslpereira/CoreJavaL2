package com.java.L2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Main {
	/*
	 * Create a file named the Numbers and populate it with 30 random numbers in the
	 * range 1 to 30 including the end points. Place 1 number per line. Open the
	 * file and print the numbers 10 per line Find and print the following. 1.
	 * Average of the numbers 2. Sum of the numbers
	 */

	private static final String MYFILE = "Numbers.txt";

	public static void main(String[] args) {
		int rangeMin = 1;
		int rangeMax = 30;
		int totalNum = 30;

		writeToFile(rangeMin, rangeMax, totalNum);
		readFile(totalNum);

	}

	private static void readFile(int totalNum) {

		try (BufferedReader br = new BufferedReader(new FileReader(MYFILE))) {
			String line;
			int sum = 0;
			line = br.readLine();
			while (line != null) {
//				System.out.println(line);
				sum += Integer.parseInt(line);
				line = br.readLine();
			}
			System.out.println("1. The average is: " + sum/totalNum);
			System.out.println("2. The sum is: " + sum);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void writeToFile(int rangeMin, int rangeMax, int totalNum) {

		Random r = new Random();
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(MYFILE, true))) {
			for (int i = rangeMin; i <= totalNum; i++) {
				String num = String.valueOf(r.nextInt(rangeMax + 1));
				bw.write(num, 0, num.length());
				bw.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
