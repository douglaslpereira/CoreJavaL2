package com.java.L2;

import java.util.Scanner;

public class Main {
	/*
	 * Write a java program to create enum type all the days of the week and a
	 * method that prints a String corresponding to the day value that passed in as
	 * argument.[Hint: Use Switch Case]
	 */

	public static void main(String[] args) {
				
		Scanner sc = new Scanner(System.in);
		System.out.print("Type a week day: ");
		int s = sc.nextInt();
		
		switch (s) {
		case 1:
			System.out.println(WeekDays.SUNDAY);
			break;
		case 2:
			System.out.println(WeekDays.MONDAY);
			break;
		case 3:
			System.out.println(WeekDays.TUESDAY);
			break;
		case 4:
			System.out.println(WeekDays.WEDNESDAY);
			break;
		case 5:
			System.out.println(WeekDays.THURSDAY);
			break;
		case 6:
			System.out.println(WeekDays.FRIDAY);
			break;
		case 7:
			System.out.println(WeekDays.SATURDAY);
			break;
	

		default:
			System.out.println("Weekday not found!");
			break;
		}
	}

}

enum WeekDays {
	MONDAY("Monday"), TUESDAY("Tuesday"), WEDNESDAY("Wednesday"), THURSDAY("Thursday"), FRIDAY("Friday"),
	SATURDAY("Saturday"), SUNDAY("Sunday");

	private String nameAsString;

	private WeekDays(String nameAsString) {
		this.nameAsString = nameAsString;
	}

	@Override
	public String toString() {
		return this.nameAsString;
	}

}

