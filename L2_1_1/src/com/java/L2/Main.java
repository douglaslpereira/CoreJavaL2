package com.java.L2;

public class Main {
	/*Write a program to calculate the number of objects created at a given point using user defined class.
	 * 
	 */

	static int count = 0;

	private Main()

	{
		count++;
	}

	public static void main(String[] args) {
		Main m1 = new Main();
		Main m2 = new Main();
		Main m3 = new Main();
		Main m4; // Won't count
		
		System.out.println("Number of objects created: " + count);

	}

}
