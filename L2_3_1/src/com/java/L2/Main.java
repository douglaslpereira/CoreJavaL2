package com.java.L2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.FormatterClosedException;
import java.util.List;
import java.util.Scanner;

public class Main {
	/*
	 * Write a program to create a sequential file that could store details about
	 * the six products. Details include product id, cost & number of items
	 * available & are provided through the keyboard. Perform following operations
	 * on it a. Compute and print the total value of all six products. b. Add new
	 * products c. Display alternate products stored in the file.
	 */
	private static final String MYFILE = "product.txt";

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Please choose a item from menu: ");
		System.out.println("1 - Calculate total value from products: ");
		System.out.println("2 - Add a new Product: ");
		System.out.println("3 - Display a product: ");
		int menu = sc.nextInt();

		ProductBuffer pf = new ProductBuffer();

		switch (menu) {
		case 1:
			ProductBuffer pr = new ProductBuffer();
			System.out.println("The total value from products are " + pr.sumValues(MYFILE));
			break;
		case 2:
			System.out.println("Please input the Product ID: ");
			int pid = sc.nextInt();
			System.out.println("Please input the Product Name: ");
			String pname = sc.next();
			System.out.println("Please input the Product Stock: ");
			int pstock = sc.nextInt();
			System.out.println("Please input the Product Value: ");
			float pval = sc.nextFloat();

			List<Products> list = new ArrayList<>();
			list.add(new Products(pid, pname, pstock, pval));
			pf.writeToFile(list, MYFILE);

			break;
		case 3:
			System.out.println("Insert the ProductId to search the product: ");
			String pName = sc.next();

			ProductBuffer pb = new ProductBuffer();
			System.out.println("ID," + "Name," + "Stock," + "Cost");
			List<Products> li = pb.readFile(MYFILE, pName);
			for (Products pro : li) {
				System.out.println(pro.getProdId() + "," + pro.getProdName() + "," + pro.getProdAvailable() + ","
						+ pro.getProdValue());
			}
			break;

		default:
			System.out.println("Invalid input!");
			break;
		}
	}

}

class ProductBuffer {
	public void writeToFile(List<Products> list, String file) {

		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, true))) {
			for (Products p : list) {
				String rec = p.getProdId() + "," + p.getProdName() + "," + p.getProdAvailable() + ","
						+ p.getProdValue();
				bw.write(rec, 0, rec.length());
				bw.newLine();
			}
		} catch (FileNotFoundException fileNotFoundException) {
			System.err.println("Error creating file.");
		} catch (FormatterClosedException formatterClosedException) {
			System.err.println("Error writing to file.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<Products> readFile(String file, String i) {
		List<Products> list = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			line = br.readLine();
			while (line != null) {
				if (line.substring(0, line.indexOf(",")).equals(i)) {
					String[] values = line.split(",");
					list.add(new Products(Integer.parseInt(values[0]), values[1], Integer.parseInt(values[2]),
							Float.parseFloat(values[3])));
				}
				line = br.readLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	public float sumValues(String file) {
		List<Products> list = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			float sum = 0;
			line = br.readLine();
			while (line != null) {
				String[] values = line.split(",");
				list.add(new Products(Integer.parseInt(values[0]), values[1], Integer.parseInt(values[2]),
						Float.parseFloat(values[3])));
				sum += Float.parseFloat(values[3]);
				line = br.readLine();
			}
			return sum;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;

	}

}

class Products {
	int prodId;
	String prodName;
	float prodValue;
	int prodAvailable;

	public Products(int prodId, String prodName, int prodAvailable, float prodValue) {
		this.prodId = prodId;
		this.prodName = prodName;
		this.prodAvailable = prodAvailable;
		this.prodValue = prodValue;
	}

	public Products() {
		// TODO Auto-generated constructor stub
	}

	public int getProdId() {
		return prodId;
	}

	public void setProdId(int prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public float getProdValue() {
		return prodValue;
	}

	public void setProdValue(float prodValue) {
		this.prodValue = prodValue;
	}

	public int getProdAvailable() {
		return prodAvailable;
	}

	public void setProdAvailable(int prodAvailable) {
		this.prodAvailable = prodAvailable;
	}

	@Override
	public String toString() {
		return "Products [prodId=" + prodId + ", prodName=" + prodName + ", prodValue=" + prodValue + ", prodAvailable="
				+ prodAvailable + "]";
	}

}