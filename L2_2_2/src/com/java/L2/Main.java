package com.java.L2;

import java.util.ArrayList;

public class Main {
	/*
	 * Create an ArrayList which will be able to store only numbers like
	 * int,float,double,etc, but not any other data type
	 */

	public static void main(String[] args) {
		

		ArrayList<Number> al = new ArrayList<>();
		al.add(5);
		al.add(23.09d);
		al.add(123456789l);
//		al.add("d");

	}

}