package com.java.L2;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Main {
	/*
	 * Write a Java program to get year and months between two dates.
	 */

	public static void main(String[] args) {
		
		String dt1 = "2016-08-31";
		String dt2 = "2018-05-25";
		
		long monthsBetween = ChronoUnit.MONTHS.between(LocalDate.parse(dt1).withDayOfMonth(1), 
				 									   LocalDate.parse(dt2).withDayOfMonth(1));
		
		long yearsBetween = ChronoUnit.YEARS.between(LocalDate.parse(dt1).withDayOfMonth(1), 
				 									 LocalDate.parse(dt2).withDayOfMonth(1));

		System.out.println("Months between " + dt1 +" and " + dt2 + " is " + monthsBetween);
		System.out.println("Years between " + dt1 +" and " + dt2 + " is " + yearsBetween);

	}

}