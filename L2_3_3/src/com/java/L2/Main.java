package com.java.L2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class Main {
	/*
	 * Write a program which copies the content of one file to a new file by
	 * removing unnecessarily spaces between words.
	 */

	private static final String ORIGINALFILE = "Original.txt";
	private static final String COPIEDFILE = "Copied.txt";

	public static void main(String[] args) {
		copyFile();
	}

	private static void copyFile() {

		try (BufferedReader br = new BufferedReader(new FileReader(ORIGINALFILE));
				BufferedWriter bw = new BufferedWriter(new FileWriter(COPIEDFILE));) {
			String line;
			line = br.readLine();
			while (line != null) {
				bw.write(line.replaceAll("\\s",""));
				line = br.readLine();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
