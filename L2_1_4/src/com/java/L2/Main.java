package com.java.L2;

import java.util.Scanner;

public class Main {
	/*
	 * Write and run AssertExample program to display below pattern with Assert
	 * disabled first and then enabled [Note : Accept number of rows from user] And
	 * also Run the program without assertion so that you can catch user errors
	 * through exceptions. If a user enters a negative value, throw a
	 * MyOwnNegativeValueEnteredException, which is an extension of
	 * ArithmeticException. The display of the error information should display the
	 * negative number that was entered. If a user enters zero, throw a
	 * MyOwnZeroValueEnteredException, which is an extension of ArithmeticException.
	 */

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Insert the size of the pattern: ");

		String st = sc.nextLine();

		try {
			int n = Integer.parseInt(st);

			assert validate(n);
			
			if (validate(n)) {
				Star.drawStar(n);
			}
		} catch (NumberFormatException e) {
			System.out.println("Enter a number! " + e.getMessage());
		}

//		assert false;

	}

	private static boolean validate(int n) {
		if (n == 0) {
			throw new MyOwnZeroValueEnteredException("Value entered is ZERO!");
		} else if (n < 0) {
			throw new MyOwnNegativeValueEnteredException("Value entered is NEGATIVE! " + n);
		}
		return true;
	}

}

class Star {

	public Star() {

	}

	public static void drawStar(int n) {
		int i, j, k;
		n++;
		for (i = 1; i <= n; i++) {
			for (j = i; j < n; j++) {
				System.out.print(" ");
			}
			for (k = 1; k < (i); k++) {
				System.out.print(" *");
			}
			System.out.println();
		}
		for (i = n - 1; i >= 1; i--) {
			for (j = n; j > i; j--) {
				System.out.print(" ");
			}
			for (k = 1; k < (i); k++) {
				System.out.print(" *");
			}
			System.out.println();
		}

	}
}

class MyOwnNegativeValueEnteredException extends ArithmeticException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	MyOwnNegativeValueEnteredException(String s) {
		System.out.println("Negative Value ERROR: " + s);
	}

}

class MyOwnZeroValueEnteredException extends ArithmeticException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	MyOwnZeroValueEnteredException(String zero) {
		System.out.println("Zero Value ERROR: " + zero);
	}

}
