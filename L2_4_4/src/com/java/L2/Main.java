package com.java.L2;

import java.util.Arrays;
import java.util.List;

public class Main {
	/*
	 * Write a java program to 1 - Create an array list to store group of student
	 * marks(99,98,97,100,92,84,80,89,90) in Student class 2 - Create a method
	 * calculateAvg() in Student class to calculate average marks of students by
	 * using stream api
	 */

	public static void main(String[] args) {
		Student s = new Student();

		List<Integer> studentsMarks = Arrays.asList(99, 98, 97, 100, 92, 84, 80, 89, 90);
		for (Integer i : studentsMarks) {
			s.setStudentMarks(i);
			System.out.println(i);
		}
		s.calculateAvg(studentsMarks);
	}

}

class Student {

	int studentMarks;

	public Student() {
		// TODO Auto-generated constructor stub
	}

	public Student(int studentMarks) {
		this.studentMarks = studentMarks;
	}

	public int getStudentMarks() {
		return studentMarks;
	}

	public void setStudentMarks(int studentMarks) {
		this.studentMarks = studentMarks;
	}

	public void calculateAvg(List<Integer> avgMarks) {
		
		avgMarks.stream() //
				.mapToInt(i -> i) //
				.average() //
				.ifPresent(avg -> System.out.println("Average is: " + avg));

	}

}